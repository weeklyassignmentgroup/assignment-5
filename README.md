# Assignment 5 - React web app

## Description
We've built an online sign language translator as a Single-Page Application using React Framework.
The web application has three main pages --> 
* /login
* /translation
* /profile

## Contributors
* [Eivind Bertelsen](https://gitlab.com/eivindTB)
* [Vebjørn Sundal](https://gitlab.com/vebsun95)
* [Øyvind Reitan](https://gitlab.com/hindrance)

## Usage
Set the env variable REACT_APP_API_KEY = PMx8QJ8IpV
Published Webpage: [https://our--text-sign-translate.herokuapp.com]
Published API: [https://ove-noroff-api.herokuapp.com/]

Users are created whenever you try to login with a new username.
Logging in will bring you to the translations page.
Here you can type any word you like and see it's translation.
If you wanna keep this for later submit the translation.
You will be able to see your previous ten translations inside your profile page,
this is accessed by clicking the profile icon.


## Dependencies
    - All that comes with npx create-react-app
    - react-router-dom
    - react-hook-form
    - react-icon
    - typescript
    - express
    - node

![component-tree](./docs/component-tree.png)
