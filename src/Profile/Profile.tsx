import './Profile.css';
import ProfileHistory from "./ProfileHistory"
import Logout from "./Logout"
import { useContext } from "react";
import { UserContext } from "../Context/UserContext";
import withAuth from '../Auth/withAuth';
import ProfileInfo from './ProfileInfo';

//10 last translation for current user
//Only need to display the text
//Button to clear the translations(delete from browser local storage)
//Return 
function  Profile() {
    const [user,] = useContext(UserContext);
    var uname = "";
    var translations: string[] = [];

    if(user!==null){ 
        uname = user.username;
        translations = user.translations;
    }

    //debugger
    return (
    <>
        <ProfileInfo uname={uname}></ProfileInfo>
        <ProfileHistory translations={translations}></ProfileHistory>
        <Logout></Logout>
    </>)
}
export default withAuth(Profile);