import { useState, useContext } from "react";
import { UserContext } from "../Context/UserContext";
import { storageDelete} from "../Storage/Storage";
import { STORAGE_KEY_USER } from "../Storage/storageKeys";

//This is a component that clear storage and redirects to login
const Logout = () => {
  const [loading] = useState(false);
  const [, setUser] = useContext(UserContext);


  const handleLogoutClick = () => {
    if(window.confirm('Are you sure?')){
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  
  return (
    <> 
      <button id="profileLogout" onClick={handleLogoutClick} disabled = {loading}>
        Logout
      </button>
    </>
)};


export default  Logout;