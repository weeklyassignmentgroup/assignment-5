import ProfileHistoryItem from "./ProfileHistoryItem"
import { UserContext } from "../Context/UserContext";
import { useState, useContext } from "react";
import { deleteTranslation } from "../API/deleteTranslation";
import { Navigate, useNavigate } from "react-router-dom";

//STATE
//var translations: string[] = [];

const ProfileHistory = (props: { translations: string[] }) =>  {

  const [user, setUser] = useContext(UserContext);
  const navigate = useNavigate();
  const[SelectedId, setSelectedId] = useState<number>(-1);
  
  //Deletes all translations for this user
  const handleProfileHistoryDeleteClick =  () => {
    deleteTranslations();
  }

  //Deletes all translations or if id is given just the specific translation for the user
  const deleteTranslations = (id : number = -1) => {
    if(user!=null){
      (async function fetchData(){
        //Splice returns no element array if no deletion
        var tempTranslations : string[] = [...user.translations].reverse();
        var deleteCount : number = 1;
        if(id<0){
          deleteCount = tempTranslations.length
          id=0;
        }
        tempTranslations.splice(id, deleteCount);
        const [error, newUser] = await deleteTranslation(user.id, tempTranslations.reverse());
        unselectAllList();
        if(error === null){
          setUser(newUser);
        }else{
          throw Error(error);
        }
        
      })()
    }
  }


  const translationHistoryId = "translationHistoryList";

  //Selects one of the translations in the profileHistory list
  const handleTranslationClicked =  (index: number, e : React.MouseEvent<HTMLLIElement>) => { 
    //just to prevent double click deselecting
    if (e.detail === 1){ 
      setSelectedId(index);
      unselectAllList();
  
      //Select ours
      e.currentTarget.className = 
        (e.currentTarget.className === "selectedHistory") ? "unselectedHistory" : "selectedHistory";
    }
  }

  //Unselects all list selections
  function unselectAllList(){
    const elList = document.getElementById(translationHistoryId);
    if(elList === null) return;
    for (var i = 0; i < elList.children.length; i++) {
      elList.children[i].className="unselectedHistory";
    }
  }

  //Redirects you to the sign language translation page with the selected word typed in
  //If there is no selection it will do nothing
  const handleGoToSelectedTranslationClicked = (e : React.MouseEvent<HTMLElement>) => {
    if(user === null) return;
    if(SelectedId===undefined) return;
    const reverseArray = [...user.translations].reverse();
    navigate("/translation?text="+reverseArray[SelectedId]);
  }

  //Just redirects back to translationPage
  const handleGoToTranslationClicked = (e : React.MouseEvent<HTMLElement>) => {
    navigate("/translation");
  }

  return (
  <section id="profileHistory">
      <h4>Your translation history</h4>
      <ul id={translationHistoryId}>
        { 
          //Todo: Add the picture versions of each
          user &&  //Side-effect of reversing array
          [...user.translations].reverse().slice(0, 10).map((t, index) =>
          <ProfileHistoryItem text={t} 
            index={index} key={index + "-" + t} onSelect={handleTranslationClicked}/>
          
          )
        }
      </ul>
      <section id="deleteHistory">
        <button onClick={handleGoToSelectedTranslationClicked}>GoToSelectedTranslation</button>
        <button onClick={handleGoToTranslationClicked}>GoToTranslation</button>
        <button onClick={handleProfileHistoryDeleteClick}>DeleteAllHistory</button>
        <button onClick={() => deleteTranslations(SelectedId)}>DeleteSelectedHistory</button>
      </section>
  </section>)
}

export default ProfileHistory;