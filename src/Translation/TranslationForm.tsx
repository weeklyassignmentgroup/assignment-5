import "./Translation.css"



interface IProps {
    setLetter: React.Dispatch<React.SetStateAction<string>>,
    submitTranslationHandler: () => void
    letter: string
}

//The component representing the input box where you type regular text
function TranslationForm({letter, setLetter, submitTranslationHandler }: IProps) {

    const onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLetter(e.target.value)
    }


    return (<>

        <form className="Form" onSubmit={(e) => e.preventDefault()}>
            <input type="text" onChange={onChangeHandler} maxLength = {40} placeholder="Write something..." value={letter}></input>
            <button className="button" type="submit" onClick={submitTranslationHandler}>Submit&rarr;</button>
        </form>
    </>)
}

export default TranslationForm;