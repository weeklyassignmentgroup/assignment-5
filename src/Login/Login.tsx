import "./Login.css"
import { useForm } from 'react-hook-form'
import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { User, UserContext } from "../Context/UserContext";
import { BsFillArrowRightCircleFill } from 'react-icons/bs'
import { storageSave } from "../Storage/Storage";
import getUserByUsername from "../API/getUserByUsername";
import WithoutAuth from "../Auth/WithoutAuth";
import createUser from "../API/createUser";

interface FormValues {
    username: string,
}

//The component that represents the login page
function Login() {

    const [error, setError] = useState<string | null>(null);

    const { register, handleSubmit, formState: { errors: formErrors } } = useForm<FormValues>();

    const [, setUser] = useContext(UserContext);

    const nav = useNavigate();

    // Fetches users with username equal to form value, if user is found it logs in
    //Makes a new user if the username is not found
    const onSubmit = (values: FormValues) => {
        getUserByUsername(values.username)
        .then(([, user]): [null, User] | Promise<[string, null] | [null, User]> => {
            if(user !== null) {
                return [null, user];
            }   
            return createUser(values.username)
            .then(([error, user]): [string, null] | [null, User] => {
                if(error !== null) {
                    return [error, null];
                }
                return [null, user];
            });
        })
        .then(([error, user]) => {
            if(error !== null) {
                setError(error);
                return;
            }
            setUser(user); // Update UserContext to store the logged in User;
            storageSave("translate-user", user.id); // Save the loggend in user in local storage.
            nav("/translation"); // Navigate to the translation page.
        });
    }

    return (
        <div className="Container">
            <form className="Form" onSubmit={handleSubmit(onSubmit)}>
                <input placeholder="What's your name?" {...register("username", { required: ("Username is required to login") })} />
                <BsFillArrowRightCircleFill  className="Submit-Btn" type="submit" onClick={handleSubmit(onSubmit)}/>
            </form>
            <p className="Error">{formErrors.username && formErrors.username.message
                || error && error}</p>
        </div>
    );
}

export default WithoutAuth(Login);